package com.q_rim.app7;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class BoxDrawingView  extends View {
  public static final String TAG = "Q---BoxDrawingView";

  private Box currBox;
  private ArrayList<Box> boxes = new ArrayList<Box>();
  private Paint boxPaint;
  private Paint backgroundPaint;

  // code method
  // Used when creating the view code
  public BoxDrawingView (Context context) {
    this(context, null);
  }

  // layout.XML method
  // Used when inflating the view from XML
  public BoxDrawingView(Context context, AttributeSet attrs) {
    super(context, attrs);

    // Paint the boxes a nice semitransplant RED (ARGB) <32.8>
    this.boxPaint = new Paint();
    this.boxPaint.setColor(0x22ff0000);

    // Paint the background off-white  <32.8>
    this.backgroundPaint = new Paint();
    this.backgroundPaint.setColor(0xfff8efe0);
  }

  // Implimenting BoxDrawingView <32.5>
  public boolean onTouchEvent(MotionEvent event) {
    PointF curr = new PointF(event.getX(), event.getY());         // get x,y coordinates of pointer
    Log.i(TAG, "Received event at x=" + curr.x + ", y=" + curr.y + ":");

    // create log of which event was triggered
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        Log.i(TAG, "  ACTION_DOWN");
        this.currBox = new Box(curr);   // keeping track of movements
        this.boxes.add(this.currBox);
        break;
      case MotionEvent.ACTION_MOVE:
        Log.i(TAG, "  Action_MOVE");
        if (this.currBox != null) {
          this.currBox.setCurrent(curr);
          invalidate();
        }
        break;
      case MotionEvent.ACTION_UP:
        Log.i(TAG, "  Action_UP");
        this.currBox = null;
        break;
      case MotionEvent.ACTION_CANCEL:
        Log.i(TAG, "  ACTION_CANCEL");
        this.currBox = null;
        break;
    }
    return true;
  }

  // Drawing on the canvus <32.9>
  @Override
  protected void onDraw(Canvas canvas) {
    // Fill the background
    canvas.drawPaint(this.backgroundPaint);

    // calculate the box size
    for (Box box : this.boxes) {
      float left = Math.min(box.getOrigin().x, box.getCurrent().x);
      float right = Math.max(box.getOrigin().x, box.getCurrent().x);
      float top = Math.min(box.getOrigin().y, box.getCurrent().y);
      float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);

      // draw red rectangle
      canvas.drawRect(left, top, right, bottom, this.boxPaint);
    }
  }
}
