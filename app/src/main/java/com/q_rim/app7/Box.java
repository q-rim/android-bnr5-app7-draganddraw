package com.q_rim.app7;

import android.graphics.PointF;

/* Defines movement within a box
*  - Origin Point - initial point of finger
*  - Current Point - current point of finger
*/

public class Box {
  private PointF origin;
  private PointF current;

  public Box(PointF origin) {
    this.origin = this.current = origin;
  }

  public PointF getCurrent() {
    return this.current;
  }

  public void setCurrent(PointF current) {
    this.current = current;
  }

  public PointF getOrigin() {
    return this.origin;
  }
}
